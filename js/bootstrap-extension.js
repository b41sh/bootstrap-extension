/*!
 * An extension of Twitter's Bootstrap
 */

+function($) {
  "use strict";

  var Confirm = function (element, options) {
    this.options        = options
    this.$body          = $(document.body)
    this.$element       = $(element)

    var $modal = $(this.options.target)

    if(!$modal.length) {

      var html = '<div class="modal modal-sm" tabindex="-1" id="' + this.options.target + '" role="confirm">'
                + '<div class="modal-content">'
                + '<div class="modal-body">' + this.options.title + '</div>'
                + '<div class="modal-footer">'
                + '<a class="btn cancel" href="#" data-dismiss="modal">取消</a>'
                + '<a class="btn btn-danger" href="' + this.options.remote + '">确定</a>'
                + '</div>'
                + '</div>'
                + '</div>'

      var $html = $(html)

      $html.appendTo(this.options.appendTo)
    }
  }

  Confirm.DEFAULTS = {
    title: '确定吗？',
    appendTo: 'body'
  }

  Confirm.prototype.show = function () {

    $('#'+this.options.target).modal('show')
  }

  function Plugin(option, target) {

    var options = $.extend({}, Confirm.DEFAULTS, option)

    var data = new Confirm(this, options)
    data.show()
  }

  var old = $.fn.confirm

  $.fn.confirm             = Plugin
  $.fn.confirm.Constructor = Confirm

  $.fn.confirm.noConflict = function () {
    $.fn.confirm = old
    return this
  }

  $(document).on('click.bse.confirm.data-api', '[data-toggle="confirm"]', function(e) {
    var $this   = $(this)
    var href = $this.attr('href')
    var $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))) // strip for ie7
    //option包含remote, title, toggle
    var option = $.extend({ remote: !/#/.test(href) && href }, $this.data())

    if ($this.is('a')) e.preventDefault()

    Plugin.call($target, option, this)
  })
}(jQuery);
